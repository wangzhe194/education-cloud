package com.education.cloud.course.service.api;

import com.education.cloud.course.common.dto.CourseRecommendListDTO;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.education.cloud.course.service.api.biz.ApiCourseRecommendBiz;
import com.education.cloud.course.common.bo.CourseRecommendBO;
import com.education.cloud.util.base.Result;

import io.swagger.annotations.ApiOperation;

/**
 *
 * 课程推荐
 *
 * @author kyh
 *
 */

@Api(value = "课程推荐", tags = "课程推荐")
@RestController
@RequestMapping(value = "/course/api/course/recommend")
public class ApiCourseRecommendController {

	@Autowired
	private ApiCourseRecommendBiz biz;

	/**
	 * 课程推荐列出接口
	 *
	 * @author kyh
	 */
	@ApiOperation(value = "课程推荐列出接口", notes = "课程推荐列出接口")
	@RequestMapping(value = "list", method = RequestMethod.POST)
	public Result<CourseRecommendListDTO> list(CourseRecommendBO courseRecommendBO) {
		return biz.list(courseRecommendBO);
	}

}
